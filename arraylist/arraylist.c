#include "arraylist.h"
#include <stdlib.h>
#include <stdio.h>


/**
 * Erzeugt eine neue Arrayliste.
 *
 * Die neue Liste ist leer und hat initiale Kapazität.
 *
 * @return Neue Liste, bei Fehler NULL.
 */
Arraylist *
arraylist_new(void)
{
	Arraylist *l = malloc(sizeof(Arraylist));
	if (l == NULL) {
		return NULL;
	}

	l->capacity = INITIAL_CAPACITY;
	l->size = 0;
	l->content = calloc(INITIAL_CAPACITY, sizeof(void *));

	if (l->content == NULL) {
		free(l);
		return NULL;
	}

	return l;
}


/**
 * Gibt den Speicher der Liste wieder frei.
 * 
 * @param l Die Liste.
 * @param cleanup Löschfunktion, die auf jeden einzelnen
 *     Knoten angewandt wird.
 */
void
arraylist_destroy(Arraylist *l, cleanup_t cleanup)
{
	if(l == NULL) return;
	
	for(int i = 0; i < l->size; i++)
		cleanup(l->content[i]);
	free(l->content);
	free(l);
}


/**
 * Erhöht die Kapazität der Liste.
 *
 * Die Kapazität wird verdoppelt. Falls das Vergrößern fehlschlägt, bleibt
 * die Liste unverändert.
 *
 * @param l Die Liste.
 *
 * @return Bei Fehlern -1, sonst 0.
 */
static int
arraylist_enlarge(Arraylist *l)
{
	size_t new_capacity = l->capacity * 2;

	void **enlarged = realloc(l->content, new_capacity*sizeof(void *));
	if (enlarged == NULL) {
		/* Liste bleibt unverändert */
		return -1;
	}
	l->capacity = new_capacity;
	l->content = enlarged;
	
	/* Neuen Speicherbereich initialisieren */
	for(int i = l->size; i < new_capacity; i++) {
		l->content[i] = NULL;
	}

	return 0;
}


/**
 * Prüft, ob Liste voll ist.
 *
 * @param list Die Liste.
 * 
 * @return 1, falls voll, sonst 0.
 */
static int
arraylist_is_full(Arraylist *list)
{
	return list->size >= list->capacity;
}


/**
 * Fügt Element in Liste ein.
 *
 * Vorhandene Elemente werden nach oben verdrängt.
 *
 * @param list Die Liste.
 * @param i Der Index, an dem eingefügt wird.
 * @param element Das Element, das eingefügt wird.
 *
 * @return Bei Fehler wird -1 zurückgegeben.
 */
int
arraylist_insert(Arraylist *list, size_t i, void *element)
{
	if (list == NULL || i < 0 || i > list->size) {
		return -1;
	}

	if (arraylist_is_full(list)) {
		if(arraylist_enlarge(list) == -1) {
			return -1;
		}
	}

	for (int j = list->size; j > i; j--) {
		list->content[j] = list->content[j-1];
	}

	list->content[i] = element;
	list->size++;

	return 0;
}


/**
 * Entfernt Element an Stelle i aus der Liste.
 * 
 * Übrige Elemente rücken nach unten nach.
 * 
 * @param list Die Liste.
 * @param i Der Index, an dem entfernt werden soll.
 * @param cleanup Funktion zum Freigeben des Elements.
 *
 * @return Bei Fehler -1, sonst 0.
 */
int
arraylist_remove(Arraylist *list, size_t i, cleanup_t cleanup)
{
	if(list == NULL || i < 0 || i > list->size || list->size == 0) return -1;

	/* Elemtent freigeben */
	if (arraylist_get(list, i) != NULL) {
		cleanup(arraylist_get(list, i));
	}

	/* Freigewordene Lücke schließen */
	for (int j = i; j < list->size-1; j++) {
		list->content[j] = list->content[j+1];
	}

	/* Ehemaliges letztes Element mit NULL überschreiben */
	list->content[list->size-1] = NULL;

	list->size--;

	return 0;
}


/**
 * Hängt ein Element hinten an die Liste.
 *
 * @param l Die Liste.
 * @param element Das Element, das angehängt wird.
 *
 * @return Gibt bei Fehlern -1 zurück, sonst 0.
 */
int
arraylist_append(Arraylist *l, void *element)
{
	return arraylist_insert(l, l->size, element);
}


 /**
  * Hängt ein Element vorne an die Liste an.
  *
  * @param list Die Liste.
  * @param element Das Element, das vorne angehängt wird.
  *
  * @return Bei Fehlern -1, sonst 0.
  */
int
arraylist_prepend(Arraylist *list, void *element)
{
	return arraylist_insert(list, 0, element);
}


/**
 * Überschreibt Wert in Liste.
 *
 * @param list Die Liste.
 * @param i Der Index, an dem geschrieben wird.
 * @param element Der Wert, der in Liste geschrieben wird.
 *
 * @return Bei Fehler -1, sonst 0.
 */
int
arraylist_set(Arraylist *list, size_t i, void *element)
{
	if(list == NULL || i < 0 || i > list->size) return -1;

	list->content[i] = element;

	return 0;
}


/**
 * Liest Wert aus Liste aus.
 *
 * @param list Die Liste.
 * @param i Der Index, an dem gelesen wird.
 *
 * @return Bei Fehler NULL, sonst vorhandener Wert.
 */
void *
arraylist_get(Arraylist *list, size_t i)
{
	if(list == NULL || i < 0 || i > list->size) return NULL;

	return list->content[i];
}
