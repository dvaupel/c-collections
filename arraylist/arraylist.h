#ifndef _ARRAYLIST_H_
#define _ARRAYLIST_H_


#include <stdlib.h>  /* für size_t */
#define INITIAL_CAPACITY 10


typedef struct arraylist_t
{
	size_t capacity;
	size_t size;
	void **content;
} Arraylist;

typedef void (*cleanup_t)(void *element);


Arraylist * arraylist_new(void);
void arraylist_destroy(Arraylist *list, cleanup_t);
int arraylist_insert(Arraylist *list, size_t i, void *element);
int arraylist_append(Arraylist *list, void *element);
int arraylist_prepend(Arraylist *list, void *element);
int arraylist_set(Arraylist *list, size_t i, void *element);
void * arraylist_get(Arraylist *list, size_t i);
int arraylist_remove(Arraylist *list, size_t i, cleanup_t);


#endif
