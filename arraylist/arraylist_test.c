#include "arraylist.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct integer_t {
	int val;
} Integer;


static Integer *
new_integer(int n)
{
	Integer *i = malloc(sizeof(Integer));
	i->val = n;
	return i;
}


void
free_integer(void *element)
{
	free(element);
}

int
main()
{
	Arraylist *list = arraylist_new();
	assert(list != NULL);

	/* 1000 Elemente anhängen */
	Integer *integer;
	for (int i =0; i<1000; i++) {
		integer = new_integer(random());
		if (arraylist_append(list, integer) == -1) {
			free(integer);
			break;
		}
	}
	assert(list->size == 1000);

	/* insert() */
	arraylist_insert(list, 900, new_integer(4));
	assert(((Integer *)list->content[900])->val == 4);

	arraylist_insert(list, 1000, new_integer(5));
	assert(((Integer *)list->content[1000])->val == 5);

	/* prepend() */
	int last_value = ((Integer *)list->content[list->size-1])->val;
	arraylist_prepend(list, new_integer(77));
	assert(((Integer *)list->content[0])->val == 77);
	assert(((Integer *)list->content[list->size-1])->val == last_value);

	int val750_before = ((Integer *)list->content[750])->val;
	int val751_before = ((Integer *)list->content[751])->val;
	arraylist_prepend(list, new_integer(0));
	int val750_after = ((Integer *)list->content[750])->val;
	int val751_after = ((Integer *)list->content[751])->val;
	assert(val751_after == val750_before);

	/* set() */
	arraylist_set(list, 123, NULL);
	assert(list->content[123] == NULL);

	arraylist_set(list, 200, new_integer(14));
	assert(((Integer *)list->content[200])->val == 14);

	/* get() */
	assert(arraylist_get(list, -1) == NULL);
	Integer *getEx = new_integer(456);
	arraylist_insert(list, 789, getEx);
	assert(arraylist_get(list, 789) == getEx);

	/* remove() */
	int old_size = list->size;
	arraylist_remove(list, 0, free_integer);
	assert(list->size == old_size-1);

	// Illegale Parameter
	assert(arraylist_insert(list, -5, new_integer(1)) == -1);
	assert(arraylist_insert(list, 10000, new_integer(1)) == -1);
	assert(arraylist_insert(NULL, 400, new_integer(1)) == -1);

	arraylist_destroy(list, free_integer);

	Arraylist *list2 = arraylist_new();
	arraylist_destroy(list2, NULL);

	return 0;
}
